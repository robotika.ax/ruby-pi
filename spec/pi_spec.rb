# frozen_string_literal: true

RSpec.describe Pi do
  it 'has a version number' do
    expect(Pi::VERSION).not_to be nil
  end
end
