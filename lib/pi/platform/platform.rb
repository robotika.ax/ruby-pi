# frozen_string_literal: true

module Pi
  module Platform
    # Platform class.
    class Platform
      attr_reader :platform_id
      attr_reader :label

      def initialize(platform_id, label)
        @platform_id = platform_id
        @label = label
      end

      RASPBERRY_PI = Platform.new('raspberrypi', 'Raspberry Pi')
    end
  end
end
